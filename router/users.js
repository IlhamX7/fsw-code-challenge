const router = require("express").Router();
const RPSController = require("../controllers/admin/RPSController");

router.get("/users/create", RPSController.createUserForm);
router.post("/users", RPSController.createUser);
router.get("/users/list", RPSController.getUserList);
router.get("/users/view/:id", RPSController.getUserDetail);
router.get("/users/update/:id", RPSController.getUserUpdate);
router.post("/users/update", RPSController.updateUser);
router.get("/users/delete/:id", RPSController.getUserDelete);

module.exports = router;