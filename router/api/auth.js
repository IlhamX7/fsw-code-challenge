const router = require("express").Router();

const authController = require("../../controllers/api/auth-controller");
const restrictJwt = require("../../middlewares/restrict-jwt");

router.post("/register", authController.registerAction);
router.post("/login", authController.loginAction);
router.post("/create-room", authController.createRoom);
router.get("/fight/:id", authController.readRoom);
router.put("/fight/:id", authController.fightAction);

router.get("/profile", restrictJwt, authController.profileAction);

module.exports = router;
