const router = require("express").Router();
const indexController = require("../controllers/indexController");

router.get("/home", indexController.home);
router.get("/games", indexController.games);

module.exports = router;