const router = require("express").Router();
const RPSController = require("../controllers/admin/RPSController");

router.get("/match/create", RPSController.createMatchForm);
router.post("/match", RPSController.createMatch);
router.get("/match/list", RPSController.getMatchList);
router.get("/match/view/:id", RPSController.getMatchDetail);
router.get("/match/update/:id", RPSController.getMatchUpdate);
router.post("/match/update", RPSController.updateMatch);
router.get("/match/delete/:id", RPSController.getMatchDelete);

module.exports = router;