const router = require("express").Router();
const home = require("./home");
const users = require("./users");
const match = require("./match");
const history = require("./history");
const admin = require("./admin");
const auth = require("./api/auth");

router.use(home);
router.use(users);
router.use(match);
router.use(history);
router.use(admin);
router.use(auth);

module.exports = router;