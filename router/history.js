const router = require("express").Router();
const RPSController = require("../controllers/admin/RPSController");

router.get("/history/create", RPSController.createHistoryForm);
router.post("/history", RPSController.createHistory);
router.get("/history/list", RPSController.getHistoryList);
router.get("/history/view/:id", RPSController.getHistoryDetail);
router.get("/history/update/:id", RPSController.getHistoryUpdate);
router.post("/history/update", RPSController.updateHistory);
router.get("/history/delete/:id", RPSController.getHistoryDelete);

module.exports = router;