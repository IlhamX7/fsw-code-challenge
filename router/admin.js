const router = require("express").Router();
const RPSController = require("../controllers/admin/RPSController");
const restrict = require("../middlewares/restrict");

router.get("/", restrict, function (req,res,next) { 
    res.redirect("/users/list");
});

router.get("/register", (req, res) => res.render("register"));
router.post("/register", RPSController.registerAction);

router.get("/login", (req, res) => res.render("login"));
router.post("/login", RPSController.loginAction);

router.get("/session", restrict, RPSController.adminSessionAction);

module.exports = router;