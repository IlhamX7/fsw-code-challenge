const { users, match } = require("../../models");
const passport = require("passport");

const registerAction = (req, res, next) => {
  console.log(req.body);
  users
    .register(req.body)
    .then((user) => {
      res.json({
        id: user.id,
        username: user.username,
      });
    })
    .catch((err) => next(err));
};

const loginAction = (req, res, next) => {
  users
    .authenticate(req.body)
    .then((user) => {
      res.json({
        id: user.id,
        username: user.username,
        token: user.generateToken(),
      });
    })
    .catch((err) => next(err));
};

const profileAction = (req, res, next) => {
  res.json(req.user);
};

const createRoom = (req, res, next) => {
  console.log(req.body);
  match
    .create({
      room_name: req.body.room_name,
    })
    .then(function (room) {
      res.json({
        id: room.id,
        room_name: room.room_name,
      });
    })
    .catch((err) => next(err));
};

const readRoom = (req, res, next) => {
  match
    .findOne({
      where: { id: req.params.id },
    })
    .then(function (room) {
      res.json({
        id: room.id,
        room_name: room.room_name,
        player_1_id: room.player_1_id,
        player_2_id: room.player_2_id,
        player_1_hand: room.player_1_hand,
        player_2_hand: room.player_2_hand,
        result: room.result,
      });
    })
    .catch((err) => next(err));
};

const fightAction = (req, res, next) => {
  const player_1_id = req.users;
  const player_2_id = req.users;
  if (!player_1_id) {
    match
      .update(
        {
          player_1_hand: req.body.hand,
        },
        {
          where: { id: req.params.id },
        }
      )
      .then(function (updateRecords) {
        if (updateRecords == 0) {
          res.send("Not found");
          return;
        }

        match
          .findOne({
            where: { id: req.params.id },
          })
          .then((user) => {
            res.json(user).json(user);
          });
      })
      .catch((err) => next(err));
  } else if (!player_2_id) {
    match
      .update(
        {
          player_2_hand: req.body.hand,
        },
        {
          where: { id: req.params.id },
        }
      )
      .then(function (updateRecords) {
        if (updateRecords == 0) {
          res.send("Not found");
          return;
        }

        match
          .findOne({
            where: { id: req.params.id },
          })
          .then((user) => {
            res.json(user).json(user);
          });
      });
  } else {
    res.send("Not found");
  }
};

module.exports = {
  registerAction,
  loginAction,
  profileAction,
  createRoom,
  readRoom,
  fightAction,
};
