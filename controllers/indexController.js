const home = (req, res) => {
  res.status(200);
  res.render("home");
};

const games = (req, res) => {
  res.status(200);
  res.render("games");
};

module.exports = {
  home,
  games,
};
