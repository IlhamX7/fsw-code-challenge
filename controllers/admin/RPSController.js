const { users, match, history, Admin } = require("../../models");
const passport = require("passport");

const createUserForm = (req, res) => {
  res.render("users/form");
};

const createUser = (req, res) => {
  users
    .create({
      name: req.body.name,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      address: req.body.address,
    })
    .then(function (user) {
      res.send(`Berhasil tambah users ${user.name}`);
    });
};

const getUserList = (req, res) => {
  users.findAll().then((user) => {
    res.render("users/list", { user });
  });
};

const getUserDetail = (req, res) => {
  users
    .findOne({
      where: { id: req.params.id },
    })
    .then((user) => {
      if (!user) {
        res.status(404).send("Not found");
        return;
      }

      res.render("users/view", { user });
    });
};

const getUserUpdate = (req, res) => {
  users
    .findOne({
      where: { id: req.params.id },
    })
    .then((user) => {
      if (!user) {
        res.status(404).send("Not found");
        return;
      }

      res.render("users/update", { user });
    });
};

const updateUser = (req, res) => {
  users
    .update(
      {
        name: req.body.name,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        address: req.body.address,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (user) {
      res.redirect("/users/list");
    });
};

const getUserDelete = (req, res) => {
  users
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/users/list");
    });
};

const registerAction = function (req, res, next) {
  Admin
    .register(req.body)
    .then(() => res.redirect("/login"))
    .catch((err) => next(err));
};

const loginAction = passport.authenticate("local", {
  successRedirect: "/",
  failureRedirect: "/login",
  failureFlash: true,
});

const adminSessionAction = (req, res, next) => {
  res.render("session", req.user.dataValues);
};

const createMatchForm = (req, res) => {
  res.render("match/form");
};

const createMatch = (req, res) => {
  match
    .create({
      player_1_id: req.body.player_1_id,
      player_2_id: req.body.player_2_id,
      player_1_hand: req.body.player_1_hand,
      player_2_hand: req.body.player_2_hand,
      result: req.body.result,
    })
    .then(function (matches) {
      res.redirect("/match/list");
    });
};

const getMatchList = (req, res) => {
  match.findAll().then((matches) => {
    res.render("match/list", { matches });
  });
};

const getMatchDetail = (req, res) => {
  match
    .findOne({
      where: { id: req.params.id },
    })
    .then((matches) => {
      if (!matches) {
        res.status(404).send("Not found");
        return;
      }

      res.render("match/view", { matches });
    });
};

const getMatchUpdate = (req, res) => {
  match
    .findOne({
      where: { id: req.params.id },
    })
    .then((matches) => {
      if (!matches) {
        res.status(404).send("Not found");
        return;
      }

      res.render("match/update", { matches });
    });
};

const updateMatch = (req, res) => {
  match
    .update(
      {
        player_1_id: req.body.player_1_id,
        player_2_id: req.body.player_2_id,
        player_1_hand: req.body.player_1_hand,
        player_2_hand: req.body.player_2_hand,
        result: req.body.result,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (match) {
      res.redirect("/match/list");
    });
};

const getMatchDelete = (req, res) => {
  match
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/match/list");
    });
};

const createHistoryForm = (req, res) => {
  res.render("history/form");
};

const createHistory = (req, res) => {
  history
    .create({
      player_win_id: req.body.player_win_id,
      player_lose_id: req.body.player_lose_id,
      date: req.body.date,
      time: req.body.time,
      score: req.body.score,
    })
    .then(function (histories) {
      res.redirect("/history/list");
    });
};

const getHistoryList = (req, res) => {
  history.findAll().then((histories) => {
    res.render("history/list", { histories });
  });
};

const getHistoryDetail = (req, res) => {
  history
    .findOne({
      where: { id: req.params.id },
    })
    .then((histories) => {
      if (!histories) {
        res.status(404).send("Not found");
        return;
      }

      res.render("history/view", { histories });
    });
};

const getHistoryUpdate = (req, res) => {
  history
    .findOne({
      where: { id: req.params.id },
    })
    .then((histories) => {
      if (!histories) {
        res.status(404).send("Not found");
        return;
      }

      res.render("history/update", { histories });
    });
};

const updateHistory = (req, res) => {
  history
    .update(
      {
        player_win_id: req.body.player_win_id,
        player_lose_id: req.body.player_lose_id,
        date: req.body.date,
        time: req.body.time,
        score: req.body.score,
      },
      {
        where: { id: req.body.id },
      }
    )
    .then(function (history) {
      res.redirect("/history/list");
    });
};

const getHistoryDelete = (req, res) => {
  history
    .destroy({
      where: { id: req.params.id },
    })
    .then(function () {
      res.redirect("/history/list");
    });
};

module.exports = {
  createUserForm,
  createUser,
  getUserList,
  getUserDetail,
  getUserUpdate,
  updateUser,
  getUserDelete,
  createMatchForm,
  createMatch,
  getMatchList,
  getMatchDetail,
  getMatchUpdate,
  updateMatch,
  getMatchDelete,
  createHistoryForm,
  createHistory,
  getHistoryList,
  getHistoryDetail,
  getHistoryUpdate,
  updateHistory,
  getHistoryDelete,
  registerAction,
  loginAction,
  adminSessionAction,
};
