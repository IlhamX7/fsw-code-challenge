"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class Admin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static #encrypt = function (password) {
      return bcrypt.hashSync(password, 10);
    };

    static register = function ({ username, password }) {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ username, password: encryptedPassword });
    };

    checkPassword = (password) => {
      return bcrypt.compareSync(password, this.password);
    };

    static authenticate = async ({ username, password }) => {
      try {
        const admin = await this.findOne({ where: { username } });
        if (!admin) {
          return Promise.reject("Invalid username & Password");
        }
        if (!admin.checkPassword(password)) {
          return Promise.reject("Invalid Username & Password");
        }
        return Promise.resolve(admin);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  Admin.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Admin",
    }
  );
  return Admin;
};
