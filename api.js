const express = require("express");
const app = express();
const port = 8000;

app.set("view engine", "ejs");
app.use(express.static("assets"));
app.use(express.urlencoded({ extended: false }));

app.use(express.json());

app.use("/", router);

app.listen(port, () => console.log(`Web App Up in http://localhost:${port}`));
