const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { users } = require("../models");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "rahasia",
    },
    async (payload, done) => {
      users
        .findByPk(payload.id)
        .then((user) => done(null, user))
        .catch((err) => done(err, false));
    }
  )
);

module.exports = passport;
