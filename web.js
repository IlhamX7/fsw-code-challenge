const express = require("express");
const app = express();
const session = require("express-session");
const flash = require("express-flash");
const passport = require("./lib/passport");
const passportJwt = require("./lib/passport-jwt");

app.use(express.urlencoded({ extended: false }));
app.set("view engine", "ejs");
app.use(express.static("assets"));
app.use(express.json());

var indexRouter = require('./router/index');
var usersRouter = require('./router/users');
var apiAuthRouter = require('./router/api/auth');

app.use(
  session({
    secret: "ini rahasia",
    resave: false,
    saveUninitialized: false,
  })
);
app.use(flash());
app.use(passportJwt.initialize());
app.use(passport.initialize());
app.use(passport.session());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/auth', apiAuthRouter);

const router = require("./router");
app.use(router);

app.listen(8000, () => console.log(`Web App Up in http://localhost:8000`));
